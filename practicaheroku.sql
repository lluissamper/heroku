﻿create table app(
app_code varchar(40) NOT NULL,
descrip varchar(40),
CONSTRAINT pk_app PRIMARY KEY (app_code)
);

create table record(
id serial NOT NULL,
app_code varchar(40),
player varchar(250),
score int,
datetime timestamp with time zone DEFAULT now(),
CONSTRAINT pk_records PRIMARY KEY (id),
CONSTRAINT fk_app FOREIGN KEY (app_code) REFERENCES app (app_code)
);
/*Insert app*/
insert into app (app_code,descrip) VALUES ('ABC','Codig app ABC');
insert into app (app_code,descrip) VALUES ('DEF','Codig app DEF');
insert into app (app_code,descrip) VALUES ('GHI','Codig app GHI');
insert into app (app_code,descrip) VALUES ('JKL','Codig app JKL');
insert into app (app_code,descrip) VALUES ('MNO','Codig app MNO');

/*select * from app*/
/*Insert record*/
insert into record (app_code,player,score) VALUES ('ABC','Lluis',9823432);
insert into record (app_code,player,score) VALUES ('DEF','Gregori',859524);
insert into record (app_code,player,score) VALUES ('GHI','Ruben',838445);
insert into record (app_code,player,score) VALUES ('JKL','Canas',65834);
insert into record (app_code,player,score) VALUES ('MNO','Toni',236755);


